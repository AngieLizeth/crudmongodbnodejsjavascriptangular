//Importar el modelo de la base de datos para el CRUD
const modeloFactura = require('../models/modelFactura');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let factura;
        factura = new modeloFactura({
        idFactura: 11111,
        FechaFacturacion: "04/marzo/2021",
        NombreCliente: "Juan"
        });
        await factura.save();
        res.send(factura);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar la factura');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const factura = await modeloFactura.find();
        res.json(factura);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener la factura.');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const factura = await modeloFactura.findById(req.params.idFactura);
        if(!factura){
            console.log(factura);
            res.status(404).json({msg: 'La factura no existe.'});
        } else {
            //por ahora solo se actualiza Nombre cliente
            await modeloFactura.findByIdAndUpdate({_id: req.params.idFactura}, {NombreCliente: "Martin"});
            res.json({msg: 'Factura actualizada correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar la factura');
    }
}

// CRUD DELETE
exports.eliminar =  async (req, res)=>{
    try {
        const factura = await modeloFactura.findById(req.params.idFactura);
        console.log(factura);
        if (!factura){
            console.log(factura);
            res.status(404).json({msg: 'La facttura no existe.'});
        } else{
            await modeloFactura.findByIdAndRemove({_id: req.params.idFactura});
            res.json({msg: 'La factura se elimino correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar la factura');
    }
}