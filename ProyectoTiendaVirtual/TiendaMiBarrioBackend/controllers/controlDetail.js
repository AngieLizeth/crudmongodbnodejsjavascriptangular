//Importar el modelo de la base de datos para el CRUD
const modeloDetalle = require('../models/modelDetalle');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let detalle;
        detalle = new modeloDetalle({
        idDetalle: 342321,
        Cantidad: 5,
        TotalPago: 4500
        });
        await detalle.save();
        res.send(detalle);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar el Detalle');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const detalle = await modeloDetalle.find();
        res.json(detalle);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el detalle.');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const detalle = await modeloDetalle.findById(req.params.idDetalle);
        if(!detalle){
            console.log(detalle);
            res.status(404).json({msg: 'El detalle no existe.'});
        } else {
            //por ahora solo se actualiza cantidad
            await modeloDetalle.findByIdAndUpdate({_id: req.params.idDetalle}, {Cantidad: 66});
            res.json({msg: 'Detalle actualizado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el detalle');
    }
}

// CRUD DELETE
exports.eliminar =  async (req, res)=>{
    try {
        const detalle = await modeloDetalle.findById(req.params.idDetalle);
        console.log(detalle);
        if (!detalle){
            console.log(detalle);
            res.status(404).json({msg: 'El detalle no existe.'});
        } else{
            await modeloDetalle.findByIdAndRemove({_id: req.params.idDetalle});
            res.json({msg: 'El detalle se elimino correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el detalle');
    }
}