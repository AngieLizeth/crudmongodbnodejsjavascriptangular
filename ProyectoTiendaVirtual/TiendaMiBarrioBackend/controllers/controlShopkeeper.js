//Importar el modelo de la base de datos para el CRUD
const modeloTendero = require('../models/modelTendero');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let tendero;
        tendero = new modeloTendero({
        idCedulaTendero: 1240540,
        Nombre: "Pedro Jose",
        Apelliddo: "Martinez",
        Email: "pedroMartinez@gmail.com",
        Usuario: "Pedro123",
        Clave: "123Pedro"
        });
        await tendero.save();
        res.send(tendero);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar el tendero');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const tendero = await modeloTendero.find();
        res.json(tendero);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el tendero');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const tendero = await modeloTendero.findById(req.params.idCedulaTendero);
        if(!tendero){
            console.log(tendero);
            res.status(404).json({msg: 'El tendero no existe.'});
        } else {
            //por ahora solo se actualiza clave
            await modeloTendero.findByIdAndUpdate({_id: req.params.idCedulaTendero}, {Clave: "123Juan"});
            res.json({msg: 'Datos de tendero actualizados correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el datos de tendero');
    }
}

// CRUD DELETE
exports.eliminar =  async (req, res)=>{
    try {
        const tendero = await modeloTendero.findById(req.params.idCedulaTendero);
        console.log(tendero);
        if (!tendero){
            console.log(tendero);
            res.status(404).json({msg: 'El tendero no existe.'});
        } else{
            await modeloTendero.findByIdAndRemove({_id: req.params.idCedulaTendero});
            res.json({msg: 'El tendero se elimino correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el tendero');
    }
}