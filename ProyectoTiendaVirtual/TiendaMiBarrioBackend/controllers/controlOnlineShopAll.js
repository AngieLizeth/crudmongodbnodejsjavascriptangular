//Importar el modelo de la base de datos para el CRUD
const modeloTiendaVirtualAll = require('../models/modelTiendaVirtualAll');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let tiendavirtualall;
        tiendavirtualall = new modeloTiendaVirtualAll({
        idCedulaTendero: 423321,
        Nombre: "Arley",
        Apelliddo: "Araque",
        Email: "brayan.arley@gmail.com",
        Usuario: "Arley",
        Clave: "Mora243",
        
        idTienda: 4321123,
        Nombre: "Los tres",
        Direccion: "Parque central",
        Telefono: 87364,
        
        idDetalle: 14563,
        Cantidad: 45,
        TotalPago: 98799,
        
        idProducto: 544213,
        Nombre: "Maiz",
        Precio: 4500,
        Stock: 34,
        Peso: "Kilos",
        FechaVencimiento: "04/enero/2022",
        Categoria: "Viveres",
        
        idFactura: 838282,
        FechaFacturacion: "03/diciembre/2021",
        NombreCliente: "Julian"
        });
        await tiendavirtualall.save();
        res.send(tiendavirtualall);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const tiendavirtualall = await modeloTiendaVirtualAll.find();
        res.json(tiendavirtualall);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener informacion');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const tiendavirtualall = await modeloTiendaVirtualAll.findById(req.params._id);
        if(!tiendavirtualall){
            console.log(tiendavirtualall);
            res.status(404).json({msg: 'No existe informacion.'});
        } else {
            //por ahora solo se actualiza clave
            await modeloTiendaVirtualAll.findByIdAndUpdate({_id: req.params._id}, {Clave: "fidjfkx"});
            res.json({msg: 'Datos actualizados correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar los datos');
    }
}

// CRUD DELETE
exports.eliminar =  async (req, res)=>{
    try {
        const tiendavirtualall = await modeloTiendaVirtualAll.findById(req.params._id);
        console.log(tiendavirtualall);
        if (!tiendavirtualall){
            console.log(tiendavirtualall);
            res.status(404).json({msg: 'No existen datos.'});
        } else{
            await modeloTiendaVirtualAll.findByIdAndRemove({_id: req.params._id});
            res.json({msg: 'Se elimino correctamente los datos.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar');
    }
}