//Importar el modelo de la base de datos para el CRUD
const modeloTienda = require('../models/modelTienda');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let tienda;
        tienda = new modeloTienda({
        idTienda: 534543,
        Nombre: "Tienda el rincon",
        Direccion: "cale 20 # 34-04",
        Telefono: 7849456
        });
        await tienda.save();
        res.send(tienda);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar los datos de la tienda');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const tienda = await modeloTienda.find();
        res.json(tienda);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener los datos de la tienda');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const tienda = await modeloTienda.findById(req.params.idTienda);
        if(!tienda){
            console.log(tienda);
            res.status(404).json({msg: 'Los datos de la tienda no existen.'});
        } else {
            //por ahora solo se actualiza telefono
            await modeloTienda.findByIdAndUpdate({_id: req.params.idTienda}, {Telefono: 7777777});
            res.json({msg: 'Datos tienda actualizados correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar datos tienda');
    }
}

// CRUD DELETE
exports.eliminar =  async (req, res)=>{
    try {
        const tienda = await modeloTienda.findById(req.params.idTienda);
        console.log(tienda);
        if (!tienda){
            console.log(tienda);
            res.status(404).json({msg: 'Los datos de la tienda no existen.'});
        } else{
            await modeloTienda.findByIdAndRemove({_id: req.params.idTienda});
            res.json({msg: 'La tienda se elimino correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar la tienda');
    }
}