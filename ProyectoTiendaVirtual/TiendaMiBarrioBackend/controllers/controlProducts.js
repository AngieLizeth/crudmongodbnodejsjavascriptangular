//Importar el modelo de la base de datos para el CRUD
const modeloProducto = require('../models/modelProducto');


//Exportar en diferentes variables los metodos para el CRUD

//CRUD => Create
exports.crear = async (req, res) => {
    try {
        let producto;
        producto = new modeloProducto({
        idProducto: 121,
        nombre: "Brocoli",
        precio: 466,
        stock: 34,
        peso: "Kl",
        fechaVencimiento: "04/julio/2023",
        categoria: "Viveres"
        });
        await producto.save();
        res.send(producto);
    } catch (error) {
        console.log("error create: ",error);
        res.status(500).send('Error al guardar el Producto');
    }
    
}

//CRUD =>READ
exports.obtener = async(req, res) =>{
    try {
        const producto = await modeloProducto.find();
        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el/los producto(s)');
    }
}

//CRUD => Update 
exports.actualizar = async(req, res) => {
    try {
        const producto = await modeloProducto.findById(req.params.idProducto);
        if(!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        } else {
            //por ahora solo se actualiza stock
            await modeloProducto.findByIdAndUpdate({_id: req.params.idProducto}, {Stock: 66});
            res.json({msg: 'Producto actualizado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el producto');
    }
}

// CRUD DELETE
exports.eliminar = async (req, res) => {
    try {
        const producto = await modeloProducto.findById(req.params.idProducto);
        console.log(producto);
        if (!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        }
        else{
            await modeloProducto.findByIdAndRemove({_id: req.params.idProducto});
            res.json({mensaje: 'Producto eliminado correctamente.'})
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el producto.');
    }
}