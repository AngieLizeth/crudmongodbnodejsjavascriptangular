//Forma convencional de ES6 => estándar convencional de JS
//import express from express
//Forma sistema nativo de NodeJS
const express = require('express');
//Api exponer
const router = express.Router();
//importar modulo a la base de datos (archivo db.js)
const conexionDB = require('./config/db');
// // Importar módulo de MongoDB => mongoose
// const mongoose = require('mongoose');

let appExpress = express();
// appExpress.use('/', function(request, response){
//     response.send("Hola mundo");
// });
// uso de archivos tipo json en la app
appExpress.use(express.json());
appExpress.use(router);
// appExpress.listen(4000);
// Habilitar el acceso al Frontend en la carpeta public
// Integración del Frontend en el Backend
appExpress.use(express.static('public'));
//COnexion a la base de datos desentralizada
conexionDB();
//Conexion con la base de datos sin variable de entorno
// const user = 'B30G3TiendaVirtualUser';
// const password = 'admin';
// const db = 'tiendaVirtualRemote';
// const url = `mongodb+srv://${user}:${password}@cluster-misiontic-tiend.9px6z.mongodb.net/${db}?retryWrites=true&w=majority`;
// // modelo esquema (schema) de la base de datos => es la estructura...
// const productoSchema = new mongoose.Schema({
//     idProducto: Number,
//     Nombre: String,
//     Precio: Number,
//     Stock: Number,
//     Peso: String,
//     FechaVencimiento: String,
//     Categoria: String
// });
// const modeloProducto = mongoose.model('collections_productos', productoSchema); 
// CRUD => Create
// modeloProducto.create(
//     {
//         idProducto: 13236,
//         Nombre: "Papa",
//         Precio: 4323,
//         Stock: 45,
//         Peso: "Kl", 
//         FechaVencimiento: "00/00/2023",
//         Categoria: "Viveres"
//     },
//     (error) => {
//         console.log("Ingresó función error...");
//         if (error) return console.log(" crete: ",error);
//         console.log(error);
//         console.log("Sale de la función error...");
//     });

// // CRUD => Read
// modeloProducto.find((error, productos) => {
//     console.log("Ingresó función error...");
//     if (error) return console.log(error);
//     console.log(productos);
// });

// // // CRUD => Update
// modeloProducto.updateOne({idProducto: 13236}, {Nombre: 'lentejas'}, (error) => {
//     if (error) return console.log("update: ", error);
// });

// // // CRUD => Delete
// modeloProducto.deleteOne({idProducto: 13236}, (error) => {
//     if (error) return console.log(error);
// });

//______________________________________
//###### Decentralizacion del CRUD #####
//######    Rutas respecto al CRUD #####   
//______________________________________
//uso de archivos tipo Json en la app
appExpress.use(express.json());



//CORS => Mecanismos o reglas de seguridad para el control de peticiones http
// Solución Temporal error CORS
const cors = require('cors');
appExpress.use(cors());
// Solicitudes al CRUD => el controlador 
const crudProductos = require('./controllers/controlProducts');
// const crudFactura = require('./controllers/controlInvoice');
// const crudDetalle = require('./controllers/controlDetail');
// const crudTienda = require('./controllers/controlShop');
// const crudTendero = require('./controllers/controlShopkeeper');
// const crudTiendaVirtualAll = require('./controllers/controlOnlineShopAll');
// var whitelist = ['http://example1.com', 'http://example2.com', 'http://localhost:4000/', 'http://localhost:4200/']
var whitelist = ['http://example1.com', 'http://example2.com']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}

// Establecer las rutas respecto al CRUD
// CRUD => Create
// router.post('/apirest/', cors(corsOptionsDelegate), crudProductos.crear);
// // CRUD => Read
// router.get('/apirest/', cors(corsOptionsDelegate), crudProductos.obtener);
// // CRUD => Update
// router.put('/apirest/:id', cors(corsOptionsDelegate), crudProductos.actualizar);
// // CRUD => Delete
// router.delete('/apirest/:id', cors(corsOptionsDelegate), crudProductos.eliminar);
router.post('/apirest', cors(corsOptionsDelegate), crudProductos.crear);
// CRUD => Read
router.get('/apirest', cors(corsOptionsDelegate), crudProductos.obtener);
// CRUD => Update
router.put('/apirest/:idProducto', cors(corsOptionsDelegate), crudProductos.actualizar);
// CRUD => Delete
router.delete('/apirest/:idProducto', cors(corsOptionsDelegate), crudProductos.eliminar);

//Establecer las rutas respecto al controlador 
//CRUD => Create
// router.post('/', crudProductos.crear);
// router.post('/', crudFactura.crear);
// router.post('/', crudDetalle.crear);
// router.post('/', crudTienda.crear);
// router.post('/', crudTendero.crear);
// router.post('/', crudTiendaVirtualAll.crear);
// // // CRUD => Read
// router.get('/',crudProductos.obtener);
// router.get('/',crudTendero.obtener);
// router.get('/',crudFactura.obtener);
// router.get('/',crudDetalle.obtener);
// router.get('/',crudTienda.obtener);
// router.get('/',crudTiendaVirtualAll.obtener);
// // // CRUD => Update
// router.put('/:idProducto',crudProductos.actualizar);
// router.put('/:idFactura',crudFactura.actualizar);
// router.put('/:idDetalle',crudDetalle.actualizar);
// router.put('/:idTienda',crudTienda.actualizar);
// router.put('/:idCedulaTendero',crudTendero.actualizar);
// router.put('/:_id',crudTiendaVirtualAll.actualizar);
// // // CRUD => Delete
// router.delete('/:idProducto',crudProductos.eliminar);
// router.delete('/:idFactura',crudFactura.eliminar);
// router.delete('/:idDetalle',crudDetalle.eliminar);
// router.delete('/:idTienda',crudTienda.eliminar);
// router.delete('/:idCedulaTendero',crudTendero.eliminar);
// router.delete('/:_id',crudTiendaVirtualAll.eliminar);









//get
// router.get('/buscarget', (req, res) =>{
//     res.send("Prueba metodo get ");
// });
//post get
// router.post('/buscarget', function(req, res){
//     res.send("Prueba metodo post con get");
// });
// //post
// router.post('/crearpost', function(req, res){
//     res.send("Prueba metodo post");
// });
//put
// router.put('/actualizarput', function(req, res){
//     res.send("Prueba metodo put");
// });
// //delete
// router.delete('/eliminardelete', function(req, res){
//     res.send("Prueba metodo delete");
// });
appExpress.listen(process.env.PORT);
console.log("La aplicacion se ejecuta en http://localhost:4000");