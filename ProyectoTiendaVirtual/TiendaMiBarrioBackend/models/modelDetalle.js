//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const detalleSchema = mongoose.Schema({
    idDetalle: Number,
    Cantidad: Number,
    TotalPago: Number 
});

//Para exponer el archivo a otras partes o archivos externos
module.exports = mongoose.model('collections_detalles', detalleSchema);

//Controlador