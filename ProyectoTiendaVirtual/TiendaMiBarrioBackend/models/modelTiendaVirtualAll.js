//Importar m[odulo de MongoDB => mongoose
// import { Schema, model } from 'mongoose';
const mongoose = require('mongoose');

const tiendaVirtualAllSchema = mongoose.Schema({
    idCedulaTendero: Number,
    Nombre: String,
    Apelliddo: String,
    Email: String,
    Usuario: String,
    Clave: String,
    idTienda: Number,
    Nombre: String,
    Direccion: String,
    Telefono: Number,
    idDetalle: Number,
    Cantidad: Number,
    TotalPago: Number,
    idProducto: Number,
    Nombre: String,
    Precio: Number,
    Stock: Number,
    Peso: String,
    FechaVencimiento: String,
    Categoria: String,
    idFactura: Number,
    FechaFacturacion: String,
    NombreCliente: String
},
{
    versionKey: false, //quitar llaves
    timestamps: true //colocar tiempo
});

//Para exponer el archivo a otras partes o archivos externos
// export default model('modelDetalle', detalleSchema);
module.exports = mongoose.model('collections_tienda_virtual_remote_alls', tiendaVirtualAllSchema);

//Controlador