//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const tiendaSchema = mongoose.Schema({
    idTienda: Number,
    Nombre: String,
    Direccion: String,
    Telefono: Number
    
});

//Para exponer el archivo a otras partes o archivos externos
module.exports = mongoose.model('collections_tiendas', tiendaSchema);

//Controlador