//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const facturaSchema = mongoose.Schema({
    idFactura: Number,
    FechaFacturacion: String,
    NombreCliente: String
    
});

//Para exponer el archivo a otras partes o archivos externos
module.exports = mongoose.model('collections_facturas', facturaSchema);

//Controlador