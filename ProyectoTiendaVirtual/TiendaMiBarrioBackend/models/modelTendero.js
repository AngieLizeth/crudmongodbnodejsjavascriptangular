//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const tenderoSchema = mongoose.Schema({
    idCedulaTendero: Number,
    Nombre: String,
    Apelliddo: String,
    Email: String,
    Usuario: String,
    Clave: String
});

//Para exponer el archivo a otras partes o archivos externos
module.exports = mongoose.model('collections_tenderos', tenderoSchema);

//Controlador