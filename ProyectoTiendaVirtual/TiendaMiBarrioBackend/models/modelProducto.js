//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    idProducto: Number,
    nombre: String,
    precio: Number,
    stock: Number,
    peso: String,
    fechaVencimiento: Date,
    categoria: String,
    urlimg: String
},
{
    versionKey: false, //quitar llaves
    timestamps: true //colocar tiempo
});

//Para exponer el archivo a otras partes o archivos externos
module.exports = mongoose.model('collections_productos', productoSchema);

//Controlador