//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');
//Importar variables de entorno
require('dotenv').config({path: 'varEntorno.env'})

//Funcion async
const conexionDB = async()=>{
    try {
    await mongoose.connect(process.env.URL_MONGODB,{});
        console.log("Conectado a MongoDB desde db.js")
    } catch (error) {
        console.error("Error en conexion a db: ", error);
        process.exit(1);
    }
}

//exportar como modulo para utilizar en otros archivos, Para que sea visible a los archivos
module.exports = conexionDB;