import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Atributos
  productos: any;

  constructor(private _productoService: ProductsService) { }

  ngOnInit(): void {
    this.obtenerProd();
  }

  // Métodos o Funciones
  obtenerProd(){
    this._productoService.obtenerDatos()
      .subscribe(datos => {
        this.productos = datos;
        console.log(this.productos);
      })
  }
}
