import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  // Atributo api(interfaz de programacion de aplicaciones)
  // url: any = 'http://localhost:4000/apirest/';
  url: any = 'http://localhost:4000/apirest/';
  // url: any = 'https://b30gxx-tiendavirtual.herokuapp.com/apirest/'; //Dominio o enlace de heroku

  constructor(private httpClient: HttpClient) { }

  // Métodos o Funciones

  // CRUD => Create
  guardarDatos(producto: any){
    return this.httpClient.post(this.url, producto);
  }

  // CRUD => Read
  obtenerDatos(){
    return this.httpClient.get(this.url);
  }

  // CRUD => Update
  actualizarDatos(id: any, producto: any){
    return this.httpClient.put(this.url + id, producto);
  }

  // CRUD => Delete
  eliminarDatos(id: any){
    return this.httpClient.delete(this.url + id)
  }
}
