import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CommonModule } from '@angular/common';

const routes: Routes =[
  // {
  //   path:'home',
  //   loadChildren: () => import('./@public/pages/home/home.module').then(modulo => modulo.HomeModule)
  // },
  // {
  //   path:'contact',
  //   loadChildren: () => import('./@public/pages/contact/contact.module').then(modulo => modulo.ContactModule)
  // },
  // {
  //   path: 'users',
  //   loadChildren:  () => import('./@admin/pages/users/users.module').then(m => m.UsersModule)
  // },
  // {
  //   path: 'dashboard',
  //   loadChildren: () => import('./@admin/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  // },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [],
  imports: [
    // CommonModule
    RouterModule.forRoot(routes, 
      {
        useHash: true,
        scrollPositionRestoration: 'enabled'
      })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }