import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //Atributos
  formEditar: boolean = false;
  _id: any;
  idProducto: any;
  nombre: any;
  precio: any;
  stock: any;
  peso: any
  fechaVencimiento: any;
  categoria: any;
  urlimg: any;
  // verd: boolean = true;

  productos: any;
  // productos: any[] = [
  //   {
  //     idProducto: "1",
  //     nombre: "as",
  //     precio: "as",
  //     stock: "as",
  //     peso: "as",
  //     fechaVencimiento: "as",
  //     categoria: "as"
  //   },
  //   {
  //     idProducto: "2",
  //     nombre: "as",
  //     precio: "as",
  //     stock: "as",
  //     peso: "asss",
  //     fechaVencimiento: "ass",
  //     categoria: "as",
  //   }
  // ]

  //Llama el servicio de la base de datos 
  constructor(private _productoService: ProductsService) { }

  ngOnInit(): void {
    this.obtenerProd();
  }

  //Metodos o funciones
  guardarProducto() {
    if(this.nombre != undefined){
      
      let ind = this.productos.length +1;
      //variable aux para add values al arreglo
      let prodAux = {
        idProducto: ind,
        nombre: this.nombre,
        precio: this.precio,
        stock: this.stock,
        peso: this.peso,
        fechaVencimiento: this.fechaVencimiento,
        categoria: this.categoria,
        urlimg: this.urlimg
      }
  
      //Agregar valores al arreglo
      this.productos.push(prodAux);
    }
  }

  // editarForm(_id: any, 
  //   id: any, 
  //   nombre: any, 
  //   precio: any, 
  //   stock: any, 
  //   peso: any, 
  //   fechaVencimiento: any,
  //   categoria: any,
  // urlimg: any){
  //   this.formEditar = true;
  //   this._id = _id;
  //   this.idProducto = id,
  //   this.nombre = nombre,
  //   this.precio = precio,
  //   this.stock = stock,
  //   this.peso = peso,
  //   this.fechaVencimiento = fechaVencimiento,
  //   this.categoria = categoria,
      // this.urlimg = urlimg;
  // }

  eliminar(indice: any){
    console.log("Dato a eliminar: ", indice);
    // this.productos.splice(indice, 1);
    this._productoService.eliminarDatos(indice)
      .subscribe(datos => {
        console.log("Dato eliminado: ", indice);
        this.obtenerProd();
      })
  }

  obtenerProd(){
    this._productoService.obtenerDatos()
      .subscribe(datos => {
        this.productos = datos;
        console.log(this.productos);
      })
  }

  // cancelar(){
  //   this.formEditar = false;
  //   this._id = "";
  //   this.id = "";
  //   this.nombre = "";
  //   this.cantidad = "";
  //   this.categoria = "";
  //   this.precio = "";
  //   this.urlimg = "";
  // }

  // actualizarProducto(){
  //   // variable aux para agregar valores al arreglo
  //   let prodAux = {
  //     id: this.id,
  //     nombre: this.nombre,
  //     cantidad: this.cantidad,
  //     categoria: this.categoria,
  //     precio: this.precio,
  //     urlimg: this.urlimg
  //   }

  // actualizarProducto(){
  //   // variable aux para agregar valores al arreglo
  //   let prodAux = {
  //     id: this.id,
  //     nombre: this.nombre,
  //     cantidad: this.cantidad,
  //     categoria: this.categoria,
  //     precio: this.precio,
  //     urlimg: this.urlimg
  //   }

  //   this._producto.actualizarDatos(this._id, prodAux)
  //     .subscribe(datos => {
  //       console.log("Producto actualizado:", datos);
  //       this.obtenerProd();
  //     })

  //   this.formEditar = false;
  //   this._id = "";
  //   this.id = "";
  //   this.nombre = "";
  //   this.cantidad = "";
  //   this.categoria = "";
  //   this.precio = "";
  //   this.urlimg = "";
  // }

}
