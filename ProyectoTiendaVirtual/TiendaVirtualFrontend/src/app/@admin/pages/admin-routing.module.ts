import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'users',
        // loadChildren:  () => import('./@admin/pages/users/users.module').then(m => m.UsersModule)
        loadChildren:  () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        // path: 'dashboard',
        path: '', //Esta es la que se tiene encuenta
        // loadChildren: () => import('./@admin/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
